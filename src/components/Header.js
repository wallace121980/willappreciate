import React, { Component } from 'react';
import Scroll from 'react-scroll';
import { stack as Menu } from 'react-burger-menu';
import Typist from 'react-typist';
import logo from '../images/Will_Appreciate.png';
import './Header.css';

let Link = Scroll.Link;

class Header extends Component {
  state = {
    open: false,
    in: true
  };

  onClose() {
    this.setState({ open: false })
  }

  render() {
    return (
      <header id="home">
        <nav>
          <div className="row">
            <img src={logo} alt="Will Appreciate logo" className="logo" />
            <a className="mobile-nav-icon"><i className="ion-navicon-round"></i></a>
            <Menu isOpen={this.state.open} noOverlay right className={ 'main-nav' }>
              <div className="main-nav-icon-close">
                <i className="ion-close-round" onClick={this.onClose.bind(this)}></i>
              </div>
              
              <Link to="home" smooth>Home</Link>
              <Link to="features" smooth>Mijn Diensten</Link>
              <Link to="works" smooth>About</Link>
              <Link to="contact" smooth>Contact</Link>
            </Menu>
          </div>
        </nav>
          <div className="hero-text-box">
            <Typist>
              <h1>Hallo ik ben Will, ik ben een developer.</h1> 
            </Typist>
          </div>
      </header>
    );
  }
}

export default Header;
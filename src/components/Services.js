import React, { Component} from 'react';

import './Services.css';

class Services extends Component {
  state = {
    active: false,
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = (event) => {
    if (event.srcElement) {
      this.setState({ active: true });
    } 
  }

  render () {
    return (
      <section className="section-features" id="features" ref="features">
        <div className="row">
          <h2 className={this.state.active ? 'animated bounceIn' : '' }>Mijn Diensten.</h2>
        </div>

        <div className="row">
          <div className={this.state.active ? 'animated fadeInLeft' : '' }>
            <div className="col span-1-of-2 box">
              <i className="ion-monitor icon-big"></i>          
              <h3>Websites</h3>
              <p>
                Tegenwoordig is een website voor ieder bedrijf niet meer weg te denken, omdat u met uw website laat zien wie u bent. 
                Het is als het ware uw digitale visitekaartje. Maar het is meer dan dat. 
                Wat tegenwoordig nog belangrijker is; met een professionele website zorgt u ervoor dat u vindbaar bent voor (potentiële) klanten en stimuleert u verkopen. 
              </p>
              <br />
              <p>
                Wanneer u er zeker van wilt zijn dat uw website uw bedrijf perfect naar voren brengt, dan sta ik voor u klaar. 
                Ik sta garant voor een professioneel resultaat en een website gemaakt met de hedendaagse technieken. 
                Zo maak ik gebruik van diverse programmeertalen, zoals HTML, CSS en Javascript. 
                Daarnaast gebruik ik de frameworks ReactJS, Redux en NodeJS.  
                Ook zorg ik ervoor dat uw uitingen perfect weergegeven worden op de PC en de mobiele device.
              </p>     
            </div>
            
          </div>
          
          <div className={this.state.active ? 'animated fadeInRight' : '' }>
            <div className="col span-1-of-2 box">
              <i className="ion-iphone icon-big"></i>          
              <h3>Apps</h3>
              <p>
                Een app biedt u de mogelijkheid om informatie, beelden en gegevens te delen met uw klanten en uw gebruikers. 
                De app die ik voor u programmeer kan zowel in de Play store als in de App store worden geplaatst. 
                Uiteraard wordt de app op maat gemaakt en wordt hij aangepast aan de huisstijl van uw organisatie!
              </p>
              <br />
              <p>
                Het programma dat ik gebruik voor het maken van de apps is het framework React Native. 
                Dit is een cross-platform framework van Facebook. 
                Door middel van JavaScript wordt het mogelijk gemaakt om een native ervaring te realiseren. 
                React Native maakt gebruik van een native user interface, waarbij de logica door Javascript geregeld wordt. 
                Grote bedrijven en organisaties zoals Facebook, Instagram, AirBnB en Tesla gebruiken dit framework.
              </p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Services;
import React, { Component } from 'react';
import axios from 'axios';
import { reduxForm, Field } from 'redux-form';
import './Form.css';

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      submit: ''
    }
  }

  renderInput = ({
    input, 
    label,
    type,
    placeholder,
    meta: { touched, error }
  }) => {
    return (
      <div className="row">
        <div className="col span-1-of-4">
          <label htmlFor="name">{label}</label>
        </div>
        <div className="col span-3-of-4">
          <input {...input} type={type} />
          { touched && error && <span className="text-danger">{error}</span> }
        </div>
      </div>
    );
  }

  renderTextArea = ({
    input, 
    label,
    type,
    placeholder,
    meta: { touched, error }
  }) => {
    return (
      <div className="row">
        <div className="col span-1-of-4">
          <label htmlFor="name">{label}</label>
        </div>
        <div className="col span-3-of-4">
          <textarea {...input} type={type} />
          { touched && error && <span className="text-danger">{error}</span> }
        </div>
      </div>
    );
  }

  handleFormSubmit = (values) => {
    const { reset } = this.props;
    const name = values.name;
    const email = values.email;
    const message = values.message;
    
    axios.post('https://apiguestbook.herokuapp.com/contact', {
      name: name,
      email: email,
      message: message
    }).then(response => {
      this.setState({ submit: 'Email verstuurd!' });
      reset();
    });
  }

  render () {
    const { handleSubmit } = this.props;

    return (
      <section className="section-form" id="contact">
        <div className="row">
          <h2>Neem Contact met mij op</h2>
        </div>
        <div className="row">
          <div className="contact-info col span-1-of-3">
            <div className="row">
              <p>Heeft u interesse om met mij samen te werken? Neem dan contact met mij op via het formulier of bel me even op!</p>
              <p>
                Will Appreciate<br/>
                Onderdijkserijweg 217<br/>
                Zwijndrecht, the Netherlands<br/>
                +31642718301
              </p>
            </div>
          </div>
          <form onSubmit={ handleSubmit(this.handleFormSubmit) } className="contact-form col span-2-of-3">
            <Field name="name" component={this.renderInput} type="text" label="Naam"  />
            <Field name="email" component={this.renderInput} type="email" label="Email"  />
            <Field name="message" component={this.renderTextArea} type="email" label="Bericht"  />
            <div className="row">
              <div className="col span-1-of-4">
                <label>&nbsp;</label>
              </div>
              <div className="col span-3-of-4">
                <input type="submit" value="Verzenden!"/>
                <span className="text-success">{this.state.submit}</span>
              </div>
            </div>
          </form>
        </div>
      </section>
    );
  }
}

const validate = values => {
  const errors = {};
  if (!values.name) {
    errors.name = 'Vul je naam in!';
  }
  if (!values.email) {
    errors.email = 'Vul je email in!';
  }
  if (!values.message) {
    errors.message = 'Vul je bericht in!';
  }
  return errors;
};

export default reduxForm({
  form: 'contact',
  validate
})(Form);
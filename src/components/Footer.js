import React from 'react';

import './Footer.css';

const Footer = () => {
  return (
    <footer>
      <div className="row">
        <ul className="social-links">
          <li><a href=""><i className="ion-social-facebook"></i></a></li>
          <li><a href=""><i className="ion-social-twitter"></i></a></li>
          <li><a href=""><i className="ion-social-googleplus"></i></a></li>
          <li><a href=""><i className="ion-social-instagram"></i></a></li>
        </ul>
      </div>

      <div className="row">
        <p>
          Copywright &copy; 2017 by Will Appreciate. All rights reserved.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
import React from 'react';
import profile from '../images/Wil-zoom-DSC_8817.jpg';
import './About.css';

const About = () => {
  return (
    <section className="section-about" id="works">
      <div className="row">
        <div className="col span-1-of-2 img-box">
          <div className="img-wrapper">
            <img src={profile} alt="Profile photo" className="img-circle" />
          </div>  
        </div>
        <div className="col span-1-of-2 steps-box-about">
          <h2>Ik ben William Schrauwen</h2>
          <p>
            Ik ben een freelance full stack developer. 
            Mijn passie ligt al vanaf kleins af aan bij computers en gadgets. 
            De laatste jaren ben ik  erg geïnteresseerd in smartphones. 
            Het was een droom voor mij om zelf een app te maken. 
            Daarom ben ik begonnen met programmeren. 
          </p>
          <br />
          <p>
            Door enthousiaste geluiden in mijn netwerk heb ik besloten van mijn hobby mijn werk te maken. 
            Ik heb hiervoor in de zorg gewerkt, dus ik ben een sociaal en communicatief persoon. 
            Ik ben ook een perfectionist dus streef altijd naar de hoogste kwaliteit als ik een website of app maak. 
            Daarnaast ben ik flexibel, zowel praktisch als qua geest. 
            Ik kan me goed verplaatsen in uw wens als klant en schakel makkelijk in onvoorziene omstandigheden.          
          </p>
        </div>
      </div>
    </section>
  );
};

export default About; 
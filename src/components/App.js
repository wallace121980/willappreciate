import React, { Component } from 'react';

import Header from './Header';
import Services from './Services';
import About from './About';
import Form from './Form';
import Footer from './Footer';

class App extends Component {
  handleEnter() {
    console.log('Enter Features');
  }

  render() {
    return (
      <div>
        <Header/>  
        <Services />
        <About />
        <Form />
        <Footer />
      </div>
    );
  }
}

export default App;

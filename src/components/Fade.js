import React from 'react';
import  { Transition } from 'react-transition-group';

const duration = 700;

const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  transitionProperty: 'opacity, transform',
  opacity: 0,
}

const transitionStyles = {
  entering: { opacity: 0, transform: 'translateX(-50%)' },
  entered:  { opacity: 1, transform: 'translateX(0)' },
  exited: { opacity: 0, transform: 'translateX(-50%)'}
};

const Fade = ({ children, in: inProp }) => (
  <Transition in={inProp} timeout={duration}>
    {(state) => (
      <div style={{
        ...defaultStyle,
        ...transitionStyles[state]
      }}>
       {children}
      </div>
    )}
  </Transition>
);

export default Fade;

import React from 'react';

import './Works.css';
import project from '../images/project.jpg';

const Works = () => {
  return (
    <section className="section-cities" id="cities">
      <div className="row">
        <h2>I hope you will appreciate my works.</h2>
      </div>
      <div className="row js--wp-3">
        <div className="col span-1-of-3 box">
          <img src={project} alt="project" />
          <h3>Project?</h3>
          <p>Ik ben beschikbaar voor werk! Als u een interessant project voor mij heeft kunt u contact met me opnemen via het contactformulier of u kunt me bellen op onderstaand nummer. En het resultaat van onze samenwerking komt hier te staan.</p> 
        </div>
        <div className="col span-1-of-3 box">
          <img src={project} alt="project" />
          <h3>Project?</h3>
          <p>Ik ben beschikbaar voor werk! Als u een interessant project voor mij heeft kunt u contact met me opnemen via het contactformulier of u kunt me bellen op onderstaand nummer. En het resultaat van onze samenwerking komt hier te staan.</p> 
        </div>
        <div className="col span-1-of-3 box">
          <img src={project} alt="project" />
          <h3>Project?</h3>
          <p>Ik ben beschikbaar voor werk! Als u een interessant project voor mij heeft kunt u contact met me opnemen via het contactformulier of u kunt me bellen op onderstaand nummer. En het resultaat van onze samenwerking komt hier te staan.</p>     
        </div>
      </div>

    </section>
  );
};

export default Works;